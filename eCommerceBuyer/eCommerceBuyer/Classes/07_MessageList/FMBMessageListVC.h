//
//  FMBMessageListVC.h
//  eCommerceBuyer
//
//  Created by Karl on 9/8/14.
//  Copyright (c) 2014 Karl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface FMBMessageListVC : UITableViewController

// --------------------------------------------------------------------------------------
@property (strong, 	nonatomic)      PFImageView *imageviewBackground;

@end
